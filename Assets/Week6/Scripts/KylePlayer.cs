﻿using UnityEngine;

public class KylePlayer : MonoBehaviour
{
    [Tooltip("How long it will take to reach the desired value")]
    [SerializeField]
    private float directionDampTime = 0.25f;

    private Animator animator;
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        float verticalInput = Input.GetAxis("Vertical");
        float horizontalInput = Input.GetAxis("Horizontal");

        // No walking backwards
        if (verticalInput < 0)
        {
            verticalInput = 0.0f;
        }

        animator.SetFloat("Speed", horizontalInput * horizontalInput + verticalInput * verticalInput);
        animator.SetFloat("Direction", horizontalInput, directionDampTime, Time.deltaTime);

        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        if (stateInfo.IsName("Base Layer.Run"))
        {
            if (Input.GetButtonDown("Jump"))
            {
                animator.SetTrigger("Jump");
            }
        }
    }
}
