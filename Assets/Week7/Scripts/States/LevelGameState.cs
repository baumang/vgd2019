﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelGameState : GameStateBase
{
    public override void StateEnter(GameStateBase previousState)
    {
        base.StateEnter(previousState);
    }

    public override void StateExit(GameStateBase nextState)
    {
        base.StateExit(nextState);
    }
}
