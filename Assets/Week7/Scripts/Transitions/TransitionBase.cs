﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TransitionBase : MonoBehaviour
{
    [SerializeField]
    protected GameStateBase targetState;
    public GameStateBase TargetState
    {
        get { return targetState; }
    }

    public abstract bool ShouldTransition();
}
